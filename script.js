
var cPushArray = new Array();
var cStep = -1;
var saved_img;
function cPush(cx) {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(cx.canvas.toDataURL());
  console.log("url = " + cx.canvas.toDataURL());
}
function save_img(cx){
  saved_img = cx.getImageData(0,0,cx.canvas.width,cx.canvas.height);
}
function redraw_img(cx){
  cx.putImageData(saved_img,0,0);
}

//a helper function to build child nodes
function elt(name, attributes) {
    var node = document.createElement(name);
    if (attributes) {
      for (var attr in attributes)
        if (attributes.hasOwnProperty(attr))
          node.setAttribute(attr, attributes[attr]);
    }
    for (var i = 2; i < arguments.length; i++) {
      var child = arguments[i];
      if (typeof child == "string")
        child = document.createTextNode(child);
      node.appendChild(child);
    }
    return node;
}
//a set to store interfaces for the user
var controls = Object.create(null);
//the function create the canva and interface to a tag
function createPaint(parent) {
  var canvas = elt("canvas", {width: 500, height: 300});
  var cx = canvas.getContext("2d");
  var toolbar = elt("div", {class: "toolbar"});
  for (var name in controls)
    toolbar.appendChild(controls[name](cx));
  cPush(cx);
  var panel = elt("div", {class: "picturepanel"}, canvas);
  parent.appendChild(elt("div", null, panel, toolbar));
}
//the interface for user to set brush Size
controls.brushSize = function(cx) {
    var select = elt("select");
    var sizes = [1, 2, 3, 5, 8, 12, 25, 35, 50, 75, 100];
    sizes.forEach(function(size) {
        select.appendChild(elt("option", {value: size},
        size + " pixels"));
    });
    select.addEventListener("change", function() {
      cx.lineWidth = select.value;
    });
    return elt("span", null, "Brush size: ", select);
};
//the interface for user to set the canva style color
controls.color = function(cx) {
    var input = elt("input", {type: "color"});
    input.addEventListener("change", function() {
      cx.fillStyle = input.value;
      cx.strokeStyle = input.value;
    });
    return elt("span", null, "Color: ", input);
};
//handle the font part
controls.fontSize = function(cx) {
  var font_size_select = elt("select");
  var font_sizes = [1, 2, 3, 5, 8, 12, 25, 35, 50, 75, 100];
  font_sizes.forEach(function(size) {
      font_size_select.appendChild(elt("option", {value: size},
      size + " pixels"));
  });
  font_size_select.addEventListener("change", function() {
    var font_args = cx.font.split(" ");
    cx.font = font_size_select.value + "px " + font_args[1]; 
  });
  return elt("span", null, "font size: ", font_size_select);
};
//
controls.font = function(cx) {
  var font_select = elt("select");
  var fonts = ["Arial", "Times-New-Roman","Helvetica","Times","Courier-New"];
  fonts.forEach(function(font) {
    font_select.appendChild(elt("option", {value: font},
    font ));
  });
  font_select.addEventListener("change", function() {
    var font_args = cx.font.split(" ");
    cx.font = font_args[0] + " "+font_select.value;  
  });
  return elt("span", null, "font :", font_select);
};


//
controls.clear = function(cx){
    var clear_button = elt("button");
    clear_button.style.width ="50px";
    clear_button.style.height ="30px";
    //console.log(clear_button);
    clear_button.addEventListener("click",function(){
        cx.clearRect(0, 0, cx.canvas.width, cx.canvas.height);
        cPush(cx);
    })
    return elt("span", null, "clear: ", clear_button);
}
//

controls.undo = function (cx) {
  var undo_button = elt("button");
  undo_button.style.width ="50px";
  undo_button.style.height ="30px";
  //console.log(undo_button);
  undo_button.addEventListener("click",function(){
    if (cStep > 0) {
      cStep--;
      var canvasPic_u = new Image();
      canvasPic_u.src = cPushArray[cStep];
      //cx.clearRect(0, 0, cx.canvas.width, cx.canvas.height);
      canvasPic_u.onload = function () {
        cx.globalCompositeOperation = "copy";   
        cx.drawImage(canvasPic_u, 0, 0);
        cx.globalCompositeOperation = "source-over";
      };
      console.log('undo,Cstep = '+ cStep);
      console.log('url = '+ cPushArray[cStep]);
    }
  
  })
  return elt("span", null, "undo: ", undo_button);
}
controls.redo = function (cx) {
  var redo_button = elt("button");
  redo_button.style.width ="50px";
  redo_button.style.height ="30px";
  //console.log(redo_button);
  redo_button.addEventListener("click",function(){
    if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic_r = new Image();
      canvasPic_r.src = cPushArray[cStep];
      cx.clearRect(0, 0, cx.canvas.width, cx.canvas.height);
      //console.log('redo,Cstep = '+ cStep);
      //console.log('url = '+ cPushArray[cStep]);
      canvasPic_r.onload = function () {
        cx.globalCompositeOperation = "copy";   
        cx.drawImage(canvasPic_r, 0, 0);
        cx.globalCompositeOperation = "source-over";
      }
    }
  })
  return elt("span", null, "redo: ", redo_button);
}
controls.upload = function (cx) {
  var upload_f = elt("input");
  upload_f.type = "file";


  upload_f.addEventListener("change",function(e) {
    if(e.target.files) {
      let imageFile = e.target.files[0]; //here we get the image file
      var reader = new FileReader();
      reader.readAsDataURL(imageFile);
      reader.onloadend = function (e) {
        var myImage = new Image(); // Creates image object
        myImage.src = e.target.result; // Assigns converted image to image object
        myImage.onload = function(ev) {
          cx.drawImage(myImage,0,0); // Draws the image on canvas
          //let imgData = myCanvas.toDataURL("image/jpeg",0.75); // Assigns image base64 string in jpeg format to a variable
        }
      }
    }
 
  });
  return elt("span", null, "upload: ", upload_f);
};
controls.download = function (cx) {
  var download_f = elt("a");
  download_f.appendChild(document.createTextNode("current pic"));
  download_f.download="dl.png";
  //download_f.href = cx.canvas.toDataURL();
  download_f.addEventListener("click",function(){
    //var img    = cx.canvas.toDataURL("image/png");
    //document.write('<img src="'+img+'"/>');

    download_f.href = cx.canvas.toDataURL();
//ttdo
  });
  return elt("span", null, "download: ", download_f);
};

//a set to store all tools
var tools = Object.create(null);
var mousePressed = false;

//a function to load and and attach event handlers to the tools
controls.tool = function(cx) {
  var select = elt("select");
  select.id = "tool_now";
  for (var name in tools)
    select.appendChild(elt("option", null, name));

  cx.canvas.addEventListener("mousedown", function(event) {
    //mousePressed = true;
    if (event.which == 1) {
        //console.log("tool"+ select.value +"used");
        tools[select.value](event, cx);
        //console.log("tool name is "+select.value);
        event.preventDefault();
    }
  });
  cx.canvas.style.cursor = 'url("imgs/pencil.png"), auto';

  select.addEventListener("change",function() {
    if(select.value == "Erase")
      cx.canvas.style.cursor = 'url("imgs/eraser.png"), auto';
    else if(select.value == "Line")
      cx.canvas.style.cursor = 'url("imgs/line.png"), auto';
    else if(select.value == "rectangle")
      cx.canvas.style.cursor = 'url("imgs/rect.png"), auto';
    else if(select.value == "circle")
      cx.canvas.style.cursor = 'url("imgs/circle.png"), auto';
    else if(select.value == "Text")
      cx.canvas.style.cursor = 'url("imgs/text.png"), auto';
    else if(select.value == "triangle")
      cx.canvas.style.cursor = 'url("imgs/triangle.png"), auto';
    else
      cx.canvas.style.cursor = 'url("imgs/pencil.png"), auto';
  });
 //for the re/un do
  cx.canvas.addEventListener("mouseup",function(event) {
    if (event.which == 1) {
      cPush(cx);
      console.log("pushed! cStep = " + cStep);
    }
  });

  return elt("span", null, "Tool: ", select);
};
//a function to compute the relative pos of event
function relativePos(event, element) {
    var rect = element.getBoundingClientRect();
    return {x: Math.floor(event.clientX - rect.left),
            y: Math.floor(event.clientY - rect.top)};
  }
//a function attach "mousemove" to some function we want
function trackDrag(onMove, onEnd) {
    function end(event) {
      removeEventListener("mousemove", onMove);
      removeEventListener("mouseup", end);
      if (onEnd)
        onEnd(event);
    }
    addEventListener("mousemove", onMove);
    addEventListener("mouseup", end);
}
function trackClick(OnDown, onEnd) {
  function end(event) {
    removeEventListener("mousedown", OnDown);
    removeEventListener("mouseup", end);
    if (onEnd)
      onEnd(event);
  }
  addEventListener("mousedown", OnDown);
  addEventListener("mouseup", end);
}
//tool line or "pencil" ,draw on the canva 
tools.Pencil = function(event, cx, onEnd) {
    cx.lineCap = "round";

    var pos = relativePos(event, cx.canvas);
    trackDrag(function(event) {
      cx.beginPath();
      cx.moveTo(pos.x, pos.y);
      pos = relativePos(event, cx.canvas);
      cx.lineTo(pos.x, pos.y);
      cx.stroke();
    }, onEnd);
};
tools.Line = function(event, cx, onEnd) {
  cx.lineCap = "round";
  save_img(cx);

  var clicked_pos =relativePos(event, cx.canvas);

  //TODO : redraw the canvas everytime the mouse move
  //and draw accroding to the crrent mouse pos again
  trackDrag(function(event) {
    redraw_img(cx);
    cx.beginPath();
    cx.moveTo(clicked_pos.x,clicked_pos.y);
    pos = relativePos(event, cx.canvas);
    cx.lineTo(pos.x, pos.y);
    cx.stroke();
  }, onEnd);
};
tools.triangle= function(event, cx, onEnd) {
  save_img(cx);

  var clicked_pos =relativePos(event, cx.canvas);

  //TODO : redraw the canvas everytime the mouse move
  //and draw accroding to the crrent mouse pos again
  trackDrag(function(event) {
    redraw_img(cx);
    cx.beginPath();
    pos = relativePos(event, cx.canvas);
    let min_x = Math.min(pos.x,clicked_pos.x);
    let min_y = Math.min(pos.y,clicked_pos.y);
    let max_x = Math.max(pos.x,clicked_pos.x);
    let max_y = Math.max(pos.y,clicked_pos.y);

    cx.moveTo((min_x+max_x)/2,min_y);
    cx.lineTo(min_x,max_y);
    cx.lineTo(max_x,max_y);
    cx.closePath();           
    cx.stroke();
  }, onEnd);
}
tools.rectangle = function(event, cx, onEnd) {
  save_img(cx);

  var clicked_pos =relativePos(event, cx.canvas);

  //TODO : redraw the canvas everytime the mouse move
  //and draw accroding to the crrent mouse pos again
  trackDrag(function(event) {
    redraw_img(cx);
    cx.beginPath();
    pos = relativePos(event, cx.canvas);
    let min_x = Math.min(pos.x,clicked_pos.x);
    let min_y = Math.min(pos.y,clicked_pos.y);
    let max_x = Math.max(pos.x,clicked_pos.x);
    let max_y = Math.max(pos.y,clicked_pos.y);
    cx.rect(min_x,min_y,max_x-min_x,max_y-min_y);
           
    cx.stroke();
  }, onEnd);
}
tools.circle = function(event, cx, onEnd) {
  save_img(cx);

  var clicked_pos =relativePos(event, cx.canvas);

  //TODO : redraw the canvas everytime the mouse move
  //and draw accroding to the crrent mouse pos again
  trackDrag(function(event) {
    redraw_img(cx);
    cx.beginPath();
    pos = relativePos(event, cx.canvas);
    let radius = Math.sqrt( Math.pow((clicked_pos.x - pos.x),2) +
                            Math.pow((clicked_pos.y - pos.y),2)  );
    cx.arc(clicked_pos.x, clicked_pos.y, radius, 0, Math.PI*2, false);
    cx.stroke();
  }, onEnd);
}


//tool "Rraser",same as "pencil",
//but set the "touched" pixel to transparent
tools.Erase = function(event, cx) {
  cx.globalCompositeOperation = "destination-out";
  tools.Pencil(event, cx, function() {
    cx.globalCompositeOperation = "source-over";
  });
};
tools.Text = function(event, cx) {
    var text = prompt("Text:", "");
    if (text) {
      var pos = relativePos(event, cx.canvas);
      //cx.font = Math.max(7, cx.lineWidth) + "px sans-serif";
      cx.fillText(text, pos.x, pos.y);
      cPush(cx);
      console.log("pushed! cStep = " + cStep);
    }
};





//simply attech the paint to a random tag
var canva_parent = document.getElementById("canva_par");
createPaint(canva_parent);
//cPush(canva_parent.canvas.getContext("2d"));
//controls.tool(cx);

