# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
    controls:
        Tool:後面有一個下拉式選單可以選擇想要用的繪圖工具

        Brush size,color,font,font size
        也都是用下拉式選單來選定想要切換到想切換到的的選項

        redo/undo/reset(clear):
        都是按下按鈕後會觸發功能

        upload:
        按下"choose file"按鈕之後選擇想上傳的圖片
        downlaod:   
        點擊"download:"後面的"current pic"就會將目前的canvas下載下來
    Tools:
        pencil:
        和brush一樣，按住滑鼠時將cursor經過的路徑加上不透明的顏色
        erase:
        和pencil，按住滑鼠時將cursor經過的路徑設為透明顏色
        line:
        會以按下滑鼠以及放開的兩點，畫上一條直線
        triangle/circle/rectangle:
        會以按下滑鼠以及放開的兩點，畫出形狀
        text:
        按下滑鼠之後會出現一個prompt讓你輸入你想要的文字
        按下enter之後就會畫到滑鼠的位置

        
        
        
        
        


### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>